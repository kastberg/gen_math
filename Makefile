all:
	python3 gen_math.py
	context questions.tex
	context answers.tex
clean:
	rm -f *.tuc *.log *.pdf *.tex
