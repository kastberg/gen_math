# coding=utf-8
import random
import sys
from math import log10, floor

FILE1 = open("questions.tex", "w")
FILE2 = open("answers.tex", "w")

old_print = print

def dprint(*asd, out='both'):
    if out == 1 or out == 'both':
        old_print(*asd, file=FILE1)
    if out == 2 or out == 'both':
        old_print(*asd, file=FILE2)
        
print = dprint
all = [ 
    # procent
    "procent",
    "procent_forandring",
    "procent_andel_till",
    # fractions
    "unknown_fraction",
    "add_fractions",
    "mult_fractions",
    "one_over",
    "inv_fractions",
    # decimal
    "ez_prod",
    "decimal_prod",
    "decimal_sum",
    "one_over_decimal",
    # units
    "area",
    "length",
    "weight",
    "volume",
    "bytes",
    # exp
    "dectoexp",
    "exptodec",
    "simpleexp"
]
procent = [
    "procent",
    "procent_forandring",
    "procent_andel_till",
]
fractions = [
    "unknown_fraction",
    "add_fractions",
    "mult_fractions",
    "one_over",
    "inv_fractions",
]
decimal = [
    "ez_prod",
    "one_over_decimal",
    "decimal_prod",
    "decimal_sum",
]
units = [
    "area",
    "length",
    "weight",
    "volume",
    "bytes",
]
exponents = [
    "dectoexp",
    "exptodec",
    "simpleexp"
]
students = {
    "Test elev 1": [],
    "Test elev 2": [],
}
def round_to_1(x):
    digits = floor(log10(abs(x)))
    if digits >= 0:
        return x
    else:
        return ("%."+str(abs(digits)) + "f") % round(x, -int(floor(log10(abs(x)))))
def bytes():
    choices = [
        "\mbox{B}",0,
        "\mbox{kB}",3,
        "\mbox{MB}",6,
        "\mbox{GB}",9,
        "\mbox{TB}",12,
    ]
    which = 2*random.randint(0,(len(choices)-1)//2)
    num = random.randint(1,10)
    other = which

    while other == which:
        other = 2*random.randint(0,(len(choices)-1)//2)

    which_str = choices[which]
    other_str = choices[other]

    answer = round_to_1(num*10**(choices[which+1]-choices[other+1]))
    print("\\placeformula")
    print("\\startformula")
    print("%d %s = \\hl[2] %s" % (num, which_str, other_str),out=1)
    print("%d %s = %s %s" % (num, which_str, answer, other_str),out=2)
    print("\\stopformula")
def length():
    choices = [
        "\mbox{mm}",-3,
        "\mbox{cm}",-2,
        "\mbox{dm}",-1,
        "\mbox{m}",0,
        "\mbox{km}",3,
        "\mbox{mil}",4,
    ]
    which = 2*random.randint(0,(len(choices)-1)//2)
    num = random.randint(1,10)
    other = which

    while other == which:
        other = 2*random.randint(0,(len(choices)-1)//2)

    which_str = choices[which]
    other_str = choices[other]

    answer = round_to_1(num*10**(choices[which+1]-choices[other+1]))
    print("\\placeformula")
    print("\\startformula")
    print("%d %s = \\hl[2] %s" % (num, which_str, other_str),out=1)
    print("%d %s = %s %s" % (num, which_str, answer, other_str),out=2)
    print("\\stopformula")
def weight():
    choices = [
        "\mbox{mg}",-3,
        "\mbox{g}",0,
        "\mbox{hg}",2,
        "\mbox{kg}",3,
    ]
    which = 2*random.randint(0,(len(choices)-1)//2)
    num = random.randint(1,10)
    other = which

    while other == which:
        other = 2*random.randint(0,(len(choices)-1)//2)

    which_str = choices[which]
    other_str = choices[other]

    answer = round_to_1(num*10**(choices[which+1]-choices[other+1]))
    print("\\placeformula")
    print("\\startformula")
    print("%d %s = \\hl[2] %s" % (num, which_str, other_str),out=1)
    print("%d %s = %s %s" % (num, which_str, answer, other_str),out=2)
    print("\\stopformula")
def volume():
    choices = [
        "\mbox{ml}",-3,
        "\mbox{cm}^3",-3,
        "\mbox{dl}",-1,
        "\mbox{dm}^3",0,
        "\mbox{l}",0,
        "\mbox{m}^3", 3,
    ]
    which = 2*random.randint(0,(len(choices)-1)//2)
    num = random.randint(1,10)
    other = which

    while other == which:
        other = 2*random.randint(0,(len(choices)-1)//2)

    which_str = choices[which]
    other_str = choices[other]

    answer = round_to_1(num*10**(choices[which+1]-choices[other+1]))
    print("\\placeformula")
    print("\\startformula")
    print("%d %s = \\hl[2] %s" % (num, which_str, other_str),out=1)
    print("%d %s = %s %s" % (num, which_str, answer, other_str),out=2)
    print("\\stopformula")
def area():
    choices = [
        "\mbox{cm}^2",-4,
        "\mbox{dm}^2",-2,
        "\mbox{m}^2", 0,
        "\mbox{km}^2",6,
    ]
    which = 2*random.randint(0,(len(choices)-1)//2)
    num = random.randint(1,10)
    other = which

    while abs(other - which) > 2 or other == which:
        other = 2*random.randint(0,(len(choices)-1)//2)

    which_str = choices[which]
    other_str = choices[other]

    answer = round_to_1(num*10**(choices[which+1]-choices[other+1]))
    print("\\placeformula")
    print("\\startformula")
    print("%d %s = \\hl[2] %s" % (num, which_str, other_str),out=1)
    print("%d %s = %s %s" % (num, which_str, answer, other_str),out=2)
    print("\\stopformula")
def exptodec():
    choices = [-6,-3,-2,-1,0,1,2,3,6]
    which = random.choice(choices)
    num = random.randint(1,10)
    answer = round_to_1(num*10**(which))
    print("\\placeformula")
    print("\\startformula")
    print("%d \cdot 10^{%d} = \\hl[2]" % (num, which),out=1)
    print("%d \cdot 10^{%d} = %s" % (num, which, answer),out=2)
    print("\\stopformula")
def dectoexp():
    choices = [-6,-3,-2,-1,0,1,2,3,6]
    which = random.choice(choices)
    num = random.randint(1,10)
    answer = round_to_1(num*10**(which))
    print("\\placeformula")
    print("\\startformula")
    print("%s = \\hl[1] \cdot 10^{\\hl[1]}" % (answer),out=1)
    print("%s = %d \cdot 10^{%d}" % (answer, num, which),out=2)
    print("\\stopformula")
def simpleexp():
    choices = [-1,0,1,2,3]
    which = random.choice(choices)
    num = random.randint(1,5)
    answer = round_to_1(num**(which))
    print("\\placeformula")
    print("\\startformula")
    print("%d^{%d} = \\hl[2]" % (num, which),out=1)
    if which == -1:
        print("%d^{%d} = \\frac{1}{%d}" % (num, which, num),out=2)
    else:
        print("%d^{%d} = %s" % (num, which, answer),out=2)
    print("\\stopformula")
def procent():
    choices = [50, 20, 10, 25, 1,2,5]
    factor  = [2, 5, 10, 4, 100, 50, 20]
    which = random.randint(0,len(choices)-1)
    num = random.randint(1,10)
    print("\\placeformula")
    print("\\startformula")
    print("%d\\%% \mbox{\,av\,} %d = \\hl[2]" % (choices[which],num*factor[which]),out=1)
    print("%d\\%% \mbox{\,av\,} %d = %d" % (choices[which],num*factor[which],num),out=2)
    print("\\stopformula")
def procent_andel_till():
    choices = [50, 20, 10, 25, 1,2,5]
    factor  = [2, 5, 10, 4, 100, 50, 20]
    which = random.randint(0,len(choices)-1)
    num = random.randint(1,5)
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{%d}{%d} = \\hl[2]\\%%" % (num*choices[which],factor[which]),out=1)
    print("\\frac{%d}{%d} = %d\\%%" % (num*choices[which],factor[which],100*num*choices[which]/factor[which]),out=2)
    print("\\stopformula")
def procent_forandring():
    choices = [50, 20, 10, 25, 1,2,5]
    factor  = [2, 5, 10, 4, 100, 50, 20]
    which = random.randint(0,len(choices)-1)
    num = random.randint(1,10)
    print("\\placeformula")
    print("\\startformula")
    if random.randint(0,1) == 0:
        print("%d \mbox{\\,ökar\\,} %d\\%% = \\hl[2]" % (num*factor[which],choices[which]),out=1)
        print("%d \mbox{\\,ökar\\,} %d\\%% = %d" % (num*factor[which],choices[which],num*factor[which]*(1 + choices[which]/100)),out=2)
    else:
        print("%d \mbox{\\,minskar\\,} %d\\%% = \\hl[2]" % (num*factor[which],choices[which]),out=1)
        print("%d \mbox{\\,minskar\\,} %d\\%% = %d" % (num*factor[which],choices[which],num*factor[which]*(1 - choices[which]/100)),out=2)
    print("\\stopformula")
def one_over_decimal():
    choices = ["0.5", "0.2", "0.1", "0.25", "0.125", "0.01","0.02", "0.05"]
    a = random.choice(choices)
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{1}{%s} = \\hl[2]" % a,out=1)
    print("\\frac{1}{%s} = %d" % (a,round(1.0/float(a))),out=2)
    print("\\stopformula")
def ez_prod():
    a,b,c,d = (random.randint(0,12),random.randint(0,12),random.randint(1,10),random.randint(-3,3))
    print("\\placeformula")
    print("\\startformula")
    print(""+str(a)+"\cdot"+str(b)+" = \\hl[2]",out=1)
    print(""+str(a)+"\cdot"+str(b)+" = "+str(a*b),out=2)
    print("\\stopformula")
def decimal_prod():
    a,b,c,d = (random.randint(-3,3),random.randint(1,10),random.randint(1,10),random.randint(-3,3))
    b = round(b*10**(a), 4)
    c = round(c*10**(d), 4)
    places = a + d
    if places < 0:
        places = -places
    else:
        places = 0
    print("\\placeformula")
    print("\\startformula")
    print(""+str(b)+"\cdot"+str(c)+" = \\hl[2]",out=1)
    print((""+str(b)+"\cdot"+str(c)+" = %."+str(places)+"f") % (b*c),out=2)
    print("\\stopformula")
def decimal_sum():
    a,b,c,d = (random.randint(-3,3),random.randint(1,10),random.randint(1,10),random.randint(-3,3))
    b = round(b*10**(a), 4)
    c = round(c*10**(d), 4)
    places = min(a,d)
    if places < 0:
        places = -places
    else:
        places = 0
    print("\\placeformula")
    print("\\startformula")
    print(""+str(b)+"+"+str(c)+" = \\hl[2]",out=1)
    print((""+str(b)+"+"+str(c)+" = %."+str(places)+"f") % (b + c),out=2)
    print("\\stopformula")
def one_over():
    a,b,c,d = (random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10))
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{1}{\\frac{"+str(c)+"}{"+str(d)+"}} = \\hl[2]",out=1)
    print("\\frac{1}{\\frac{"+str(c)+"}{"+str(d)+"}} = \\frac{%d}{%d}" % (d,c),out=2)
    print("\\stopformula")
def inv_fractions():
    a,b,c,d = (random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10))
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{\\frac{"+str(a)+"}{"+str(b)+"}}{\\frac{"+str(c)+"}{"+str(d)+"}} = \\hl[2]", out=1)
    print("\\frac{\\frac{"+str(a)+"}{"+str(b)+"}}{\\frac{"+str(c)+"}{"+str(d)+"}} = \\frac{%d}{%d}" % (d*a,b*c), out=2)
    print("\\stopformula")
def mult_fractions():
    a,b,c,d = (random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10))
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{"+str(a)+"}{"+str(b)+"} \cdot \\frac{"+str(c)+"}{"+str(d)+"} = \\hl[2]",out=1)
    print("\\frac{"+str(a)+"}{"+str(b)+"} \cdot \\frac{"+str(c)+"}{"+str(d)+"} = \\frac{%d}{%d}" % (a*c,b*d),out=2)
    print("\\stopformula")
def add_fractions():
    a,b,c,d = (random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10))
    print("\\placeformula")
    print("\\startformula")
    print("\\frac{"+str(a)+"}{"+str(b)+"} + \\frac{"+str(c)+"}{"+str(d)+"} = \\hl[2]",out=1)
    print("\\frac{"+str(a)+"}{"+str(b)+"} + \\frac{"+str(c)+"}{"+str(d)+"} = \\frac{%d}{%d}"% (d*a+b*c, b*d),out=2)
    print("\\stopformula")
    
def unknown_fraction():
    r = random.randint(0,1)
    a,b,c,d,e = (random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10),random.randint(1,10))
    print("\\placeformula")
    print("\\startformula")
    if r == 0:
        a = c*e
        b = d*e
        print("\\frac{\\mframed{\\,\\,\\,\\,\\,\\,}}{"+str(b)+"} = \\frac{"+str(c)+"}{"+str(d)+"}",out=1)
        print("\\frac{"+str(a)+"}{"+str(b)+"} = \\frac{"+str(c)+"}{"+str(d)+"}",out=2)
    if r == 1:
        a = c*e
        b = d*e
        print("\\frac{"+str(a)+"}{\\mframed{\\,\\,\\,\\,\\,\\,}} = \\frac{"+str(c)+"}{"+str(d)+"}",out=1)
        print("\\frac{"+str(a)+"}{"+str(b)+"} = \\frac{"+str(c)+"}{"+str(d)+"}",out=2)
    print("\\stopformula")
    
print("\\setupbodyfont[11pt]")
print("\\setupformulas[align=right]")
print("\\startcolumns[n=3]")
print("\\setuphead[chapter][numbercommand={}]")
for student in sorted(students.keys()):
    print("\\page[odd]")
    # results = ", ".join(map(str, sorted(history[student])[-5:]))
    print(student)
    #print(results)
    print("\\setnumber[formula][0]")

    tasks = filter(lambda x: x not in students[student], all)
    tasks = list(tasks)
    sys.stdout.write("tasks: %s, student: %s\n" % (repr(tasks), repr(student)))
    #print(str(tasks).replace("[","").replace("]",""))
    for i in range(len(all)):
        eval(all[i] + "()")
    if len(tasks) <= 2:
        print("\\stopcolumns")
        print("Bra jobbat! Du ligger i fas. Jobba med blandade övningar på s. 116-119 i boken istället. Du får också sluta när du är klar med det.")
        print("\\page[even]")
        print("\\startcolumns[n=3]")
    else:
        for i in range(50-len(all)):
            eval(tasks[i%len(tasks)] + "()")
        print("\\page[even]")
        for i in range(50):
            eval(tasks[i%len(tasks)] + "()")
print("\\stopcolumns")
